## Why not ManagedIV? ##

Unmanaged IV Encryption is used for guest users where the Salesforce ManagedIV Encryption does not work. We can have our own IV and use that to encrypt and decrypt the data.


# IMPORTANT #

* If you are passing Id to URL and accessing that, use **encodeURIComponent** to encode the URL data.
* If **encodeURIComponent** is not used, then the decrytpion might fail for few scenarios discussed below:

     * If encrypted data contains + or /, then on URL they will be encoded as %2.
	 * %2 is recognised as a space character and during decryption, your data will have a space, which is not the case.
	 
	 * For example: 
	     * If encrypted data = **YGSTuay785+msie/1IOais**
		 * Then on URL, it will be something like: **https://....?Id=YGSTuay785%2msie%21IOais**
		 * If you fetch the url param Id, you will now have data as: **YGSTuay785 msie 1IOais** ( data with 2 spaces, instead of + and / ).
		 * Since original data and the new data are not same, the decryption will fail, resulting in "Error Closing Stream" error message.
		 * To overcome this, pass the Id to apex using **encodeURIComponent** method, which will preserve the identity, and decrytion will be successfull.