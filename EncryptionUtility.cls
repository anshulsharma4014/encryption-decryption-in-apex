public with sharing class EncryptionUtility {
    public static String encryptKey(String textToEncrypt){
        try {
            Blob key = Constants.ENCRYPTION_KEY;    //Typical Key: t6w9z$C&F)J@NcRfUjXnZr4u7x!A%D*G
            Blob iv = Constants.ENCRYPTION_IV;  //Typical IV: !z%C*F-JaNdRgUkXn2r5u8x/A?D(G+Kb
            Blob data = Blob.valueOf(textToEncrypt);
            Blob encryptedData = Crypto.encrypt('AES256', key, iv, data);
            String encodedData = EncodingUtil.base64Encode(encryptedData);
            String urlEncodedData = EncodingUtil.urlEncode(encodedData,'UTF-8');
            return urlEncodedData;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public static String decryptKey(String encryptedText) {
        try {
            Blob key = Constants.ENCRYPTION_KEY;
            Blob iv = Constants.ENCRYPTION_IV;
            String urlDecodedData = EncodingUtil.urlDecode(encryptedText,'UTF-8');
            Blob encodedData = EncodingUtil.base64Decode(urlDecodedData);
            Blob decryptedData = Crypto.decrypt('AES256', key, iv, encodedData);
            String decryptedString = decryptedData.toString();
            return decryptedString;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}