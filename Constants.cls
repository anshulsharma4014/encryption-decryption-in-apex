public class Constants {
    public static final Blob ENCRYPTION_KEY = Blob.valueOf('t6w9z$C&F)J@NcRfUjXnZr4u7x!A%D*G');
    public static final Blob ENCRYPTION_IV = Blob.valueOf('!z%C*F-JaNdRgUkXn2r5u8x/A?D(G+Kb');
}